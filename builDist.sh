#!/bin/bash

rm -rf ./dist
npm install
npm run build
npm install --omit dev   
cp -R ./node_modules dist 
cd ./dist
zip -r ../ao-socket.zip ./*
cd ../.aws/terraform && terraform apply
