// NOTE: execution time of the policy in nano-seconds.

var policyNames = [
  , 'fcErrorHandling'
  , 'FcLoggingPostClient'
  , 'fcOAuth2'
  , 'fcProtectScope'
  , 'fcWhiteListData'
  , 'jsLogTimeTakenCompletePolicies'
  , 'rfEmpty404'
];

function readSinglePolicy(policyName) {
  var s = policyName + ' ';
  var v = context.getVariable('apigee.metrics.policy.' + policyName + '.timeTaken');
  s += (v) ? v : '-unknown-';
  return s;
}
var result = policyNames.map(readSinglePolicy).join(',');
context.setVariable('bigString', result);