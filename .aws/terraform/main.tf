data "aws_caller_identity" "current" {}

module "resource_info" {
  source              = "module-registry.services.ally.com/ally/name-and-tags/aws"
  version             = "~> 1.0"
  application_name    = var.application_name
  service_name        = var.service_name
  workspace           = terraform.workspace
  environment         = var.environment
  application_id      = var.application_id
  data_classification = var.data_classification
  issrcl_level        = var.issrcl_level
  owner               = var.owner
  scm_project         = var.scm_project
  scm_repo            = var.scm_repo
  scm_branch          = var.scm_branch
  scm_commit_id       = var.scm_commit_id
  additional_tags = {
    "tf_starter"       = var.creation_date
    "cloud_pattern_id" = "CSP-6-1"
    service            = "${var.application_name}-${var.service_name}"
  }
}

# Documentation: https://bitbucket.int.ally.com/projects/TF/repos/terraform-modules-aws-state/browse
module "state" {
  source        = "module-registry.services.ally.com/ally/state/aws"
  version       = "~> 2.0"
  vpc_cidr_code = lookup(var.awsacct_cidr_code, var.environment)
}

module "ao-socket-lambda" {
  source                  = "module-registry.services.ally.com/ally/lambda/aws"
  version                 = "~> 7.0"
  namespace               = module.resource_info.lower_resource_name
  tags                    = module.resource_info.tags
  short_tags              = module.resource_info.short_tags
  name                    = var.service_name
  code                    = "../../ao-socket.zip"
  handler                 = "dist/lambda.handler"
  timeout                 = 30
  lambda_insights         = true
  lambda_insights_version = 18
  runtime                 = "nodejs16.x"
  code_requires_zipping   = false
  size                    = "m"
  vpc_cidr_code           = lookup(var.awsacct_cidr_code, var.environment)
  tracing_mode            = "Active"
  warmer_enabled          = false
  env_vars = {
    DYNAMODB_TABLE_NAME = module.socket_table.table.id
    WHISPER_LOG_LEVEL   = lookup(var.log_level, var.environment, (lookup(var.log_level, "default")))
  }
  execution_policy = data.aws_iam_policy_document.lambda-execution-role-policy-ao-socket.json
}

data "aws_iam_policy_document" "lambda_policy" {
  statement {
    sid    = "AllowDynamo"
    effect = "Allow"
    actions = [
      "dynamodb:*"
    ]
    resources = [
      module.socket_table.table.arn
    ]
  }

  statement {
    sid    = "AllowDynamoKMS"
    effect = "Allow"
    actions = [
      "kms:decrypt",
      "kms:encrypt"
    ]
    resources = [
      "arn:aws:kms:us-east-1:${data.aws_caller_identity.current.account_id}:alias/aws/dynamodb"
    ]
  }

}

#region - local variables
locals {
  remote_state_key = "environments/${var.region}/${lookup(var.awsacct_cidr_code, var.environment)}/terraform.tfstate"
  api_name         = "${module.resource_info.lower_resource_name}-API"
}
