# Name & Tagging
application_id   = "105198"
api_version      = "V1.0.0"
application_name = "ao"
service_name     = "socket"

owner               = "Golden Grahams"
data_classification = "Confidential"
issrcl_level        = "Medium Low"
scm_project         = "allyfinancial/ally-digital/applications/api/account-opening"
scm_repo            = "ao-socket"

api_endpoint_type               = ["PRIVATE"]
team_email                      = "agileteam-accountopeninggoldengrahams@Ally.com"
s3_package_bucket_acl           = "private"
s3_package_bucket_versioning    = true
s3_package_bucket_force_destroy = true
dlq_visibility_timeout_seconds  = 180

awsacct_cidr_code = {
  default = "010-073-240-000"
  dev     = "010-073-240-000"
  qa      = "010-072-016-000"
  cap     = "010-072-016-000"
  psp     = "010-072-048-000"
  prod    = "010-072-048-000"
}

hash_key = {
  name = "pk"
  type = "S"
}

api_basepath = "/ao-socket"
