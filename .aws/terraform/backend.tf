terraform {
  backend "s3" {
    bucket         = "ally-us-east-1-826442376546-tf-states"
    key            = "999999-ao-socket/terraform.tfstate"
    dynamodb_table = "ally-us-east-1-826442376546-tf-locks"
    region         = "us-east-1"
    encrypt        = true
  }
}

