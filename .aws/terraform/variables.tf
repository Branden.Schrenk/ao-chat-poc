variable "region" {
  type        = string
  description = "The target AWS region to deploy to."
  default     = "us-east-1"
}

variable "api_version" {
  type        = string
  description = "The target AWS region to deploy to."
  default     = "V1.0.0"
}

variable "apigee_mgmt_url" {
  type        = string
  description = "The URL of the apigee management api (defaults to Apigee's SaaS)"
  default     = "https://api.enterprise.apigee.com"
}


variable "environment" {
  type        = string
  description = "Env to use for resolving variables"
  default     = "default"
}

variable "application_id" {
  type        = string
  description = "The Ally Application ID of the project."
}

variable "application_name" {
  type        = string
  description = "The Ally Application Name of the project."
}

variable "service_name" {
  type        = string
  description = "The Ally Application service name."
}

variable "data_classification" {
  type        = string
  description = "The Ally data classification."
}

variable "issrcl_level" {
  type        = string
  description = "The Information System Security Risk Classification Level for the resource. This value should come from the ServiceNow application catalog"
}

variable "owner" {
  type        = string
  description = "Team owner of the application"
}

variable "scm_project" {
  type        = string
  description = "The Bitbucket project used to deploy the resource."
}

variable "scm_repo" {
  type        = string
  description = "The Bitbucket repository used to deploy the release."
}

variable "scm_branch" {
  type        = string
  description = "The Bitbucket and tag used to deploy the resource."
  default     = "local-deploy"
}

variable "scm_commit_id" {
  type        = string
  description = "The specific commit that was used to deploy the resource."
  default     = "000000"
}

variable "creation_date" {
  type        = string
  description = "The date and time this project was generated."
  default     = "2022-06-08T15:02:09.612336"
}

variable "api_endpoint_type" {
  type        = list(any)
  description = "The endpoint type for API endpoints"
}

variable "awsacct_cidr_code" {
  type        = map(any)
  description = "The CIDR block assigned to the account VPC"
}

variable "hash_key" {
  description = "The name and type of the hash/partition key for the table. Acceptable types are `S` (string), `N` (number), and `B` (binary)."
  type = object({
    name = string
    type = string
  })
  default = {
    name = "id"
    type = "S"
  }

  validation {
    condition     = var.hash_key.type == "S" || var.hash_key.type == "N" || var.hash_key.type == "B"
    error_message = "`hash_key.type` must be one of S, N, or B."
  }
}

variable "range_key" {
  description = "The name and type of the range/sort key for the table. Acceptable types are `S` (string), `N` (number), and `B` (binary)."
  type = object({
    name = string
    type = string
  })
  default = null

  # Use "conditional expression" so the check short cirtuits if var.range_key is null
  validation {
    condition     = var.range_key == null ? true : (var.range_key.type == "S" || var.range_key.type == "N" || var.range_key.type == "B")
    error_message = "`range_key.type` must be one of S, N, or B."
  }
}

variable "ttl_attribute_name" {
  description = "The (optional) ttl attribute name"
  type        = string
  default     = null
}

variable "ttl_enabled" {
  description = "Whether or not TTL is enabled. Only applicable if `ttl_attribute_name` is used."
  type        = bool
  default     = true
}

variable "gsi_map" {
  description = "Additional global secondary indexes in the form of a list of mapped values. You must define the `hash_key` and (optional) `range_key` in `index_attributes` when creating a GSI. Refer to the AWS provider [docs](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/dynamodb_table#nested-fields) for more information."
  type        = any
  default     = []
}

variable "lsi_map" {
  description = "Additional local secondary indexes in the form of a list of mapped values. You must define the `range_key` in `index_attributes` when creating a LSI. Refer to the AWS provider [docs](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/dynamodb_table#nested-fields) for more information."
  type        = any
  default     = []

  validation {
    condition     = length(var.lsi_map) == 0 || (can(var.lsi_map[*].projection_type) && can(var.lsi_map[*].range_key) && can(var.lsi_map[*].name))
    error_message = "Object must include `name`, `range_key`, and `projection_type` properties."
  }
}

variable "autoscaling_read" {
  description = "A map of table read autoscaling settings. `max_capacity` is the only required key. Other allowable keys are `scale_in_cooldown`, `scale_out_cooldown`, and `target_value`. (If these optional key-value pairs are not set, `autoscaling_defaults` will be used.) These same settings will also automatically be applied to every GSI on the table. See the example for more details."
  type        = map(string)
  default     = {}

  validation {
    condition     = length(var.autoscaling_read) == 0 || lookup(var.autoscaling_read, "max_capacity", null) != null
    error_message = "`max_capacity` may not be null."
  }
}

variable "autoscaling_write" {
  description = "A map of table write autoscaling settings. `max_capacity` is the only required key. Other allowable keys are `scale_in_cooldown`, `scale_out_cooldown`, and `target_value`. (If these optional key-value pairs are not set, `autoscaling_defaults` will be used.) These same settings will also automatically be applied to every GSI on the table. See the example for more details."
  type        = map(string)
  default     = {}

  validation {
    condition     = length(var.autoscaling_write) == 0 || lookup(var.autoscaling_write, "max_capacity", null) != null
    error_message = "`max_capacity` may not be null."
  }
}

variable "index_attributes" {
  description = "DynamoDB attributes in the form of a list of mapped values. These must only be defined for attributes in LSIs and GSIs."
  type = list(object({
    name = string
    type = string
  }))
  default = []
}

variable "log_level" {
  type        = map(any)
  description = "Log level for different env"
  default = {
    "default" = "info",
    "dev"     = "debug",
    "qa"      = "debug",
    "cap"     = "debug",
    "stage"   = "debug",
    "psp"     = "debug",
    "prod"    = "debug"
  }
}

variable "team_email" {
  type        = string
  description = "email of Support team"
  default     = "agileteam-accountopeningluckycharms@Ally.com"
}

variable "team_name" {
  type        = string
  description = "email of Support team"
  default     = "Golden Grahams"
}

variable "apigee_ips" {
  type        = map(any)
  description = "Map of Apigee IPs to allow access to the REST API on the AWS API Gateway"
  default = {
    "default" = [
      "35.167.139.191",
      "35.165.223.249",
      "34.204.20.17",
      "54.156.250.213"
    ],
    "psp" = [
      "35.166.41.234",
      "34.211.132.98",
      "3.217.69.172",
      "3.82.177.193"
    ],
    "prod" = [
      "35.166.41.234",
      "34.211.132.98",
      "3.217.69.172",
      "3.82.177.193"
    ]
  }
}

variable "application_contextRoot" {
  type        = string
  description = "Context root for status tracker API"
  default     = ""
}

variable "data_trace_enabled" {
  type        = map(any)
  description = "Controls cloud watch request data tracing for different environments"
  default = {
    "default" = false,
    "dev"     = true,
    "qa"      = true,
    "cap"     = true,
    "stage"   = true,
    "psp"     = false,
    "prod"    = false
  }
}

variable "cloudwatch_log_level" {
  type        = map(any)
  description = "API Gateway cloudwatch log levels for different env"
  default = {
    "default" = "INFO",
    "dev"     = "INFO",
    "qa"      = "INFO",
    "cap"     = "INFO",
    "stage"   = "INFO",
    "psp"     = "ERROR",
    "prod"    = "ERROR"
  }
}

variable "gw_access_log_format" {
  type        = string
  description = "Gw custom access log format"
  default     = "{    \"stage\" : \"$context.stage\",    \"request_id\" : \"$context.requestId\",    \"api_id\" : \"$context.apiId\",    \"resource_path\" : \"$context.resourcePath\",    \"resource_id\" : \"$context.resourceId\",    \"http_method\" : \"$context.httpMethod\",    \"source_ip\" : \"$context.identity.sourceIp\",  \"integration-status\": \"$context.integration.status\", \"integration-latency\": \"$context.integration.latency\", \"response-status\":\"$context.status\", \"response-latency\": \"$context.responseLatency\"}"
}

variable "api_basepath" {
  type        = string
  description = "The base path identified for API Products in the API Product Domain directly inform the base path for your Apigee proxy"
}

