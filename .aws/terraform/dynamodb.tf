# Documentation: https://bitbucket.int.ally.com/projects/TF/repos/terraform-modules-aws-dynamodb/browse
module "socket_table" {
  source             = "module-registry.services.ally.com/ally/dynamodb/aws"
  version            = "~> 3.0"
  namespace          = module.resource_info.lower_short_name
  name               = var.service_name
  environment        = var.environment
  hash_key           = var.hash_key
  range_key          = var.range_key
  ttl_attribute_name = var.ttl_attribute_name
  ttl_enabled        = var.ttl_enabled
  gsi_map            = var.gsi_map
  lsi_map            = var.lsi_map
  autoscaling_read   = var.autoscaling_read
  autoscaling_write  = var.autoscaling_write
  index_attributes   = var.index_attributes
  tags               = module.resource_info.tags
}
