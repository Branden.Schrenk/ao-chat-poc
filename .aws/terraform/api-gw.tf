resource "aws_apigatewayv2_api" "api_gateway" {
  name                       = "${var.application_name}-${var.service_name}-${var.application_id}"
  description                = var.application_name
  protocol_type              = "WEBSOCKET"
  tags                       = module.resource_info.tags
  route_selection_expression = "$request.body.action"
}

resource "aws_apigatewayv2_stage" "staging" {
  name        = "staging"
  api_id      = aws_apigatewayv2_api.api_gateway.id
  auto_deploy = true
}

resource "aws_apigatewayv2_integration" "lambda_hello" {
  api_id             = aws_apigatewayv2_api.api_gateway.id
  integration_uri    = module.ao-socket-lambda.invoke_arn
  integration_type   = "AWS_PROXY"
  integration_method = "POST"
}

resource "aws_apigatewayv2_route" "connect" {
  api_id    = aws_apigatewayv2_api.api_gateway.id
  route_key = "$connect"
  target    = "integrations/${aws_apigatewayv2_integration.lambda_hello.id}"
}

resource "aws_apigatewayv2_route" "disconnect" {
  api_id    = aws_apigatewayv2_api.api_gateway.id
  route_key = "$disconnect"
  target    = "integrations/${aws_apigatewayv2_integration.lambda_hello.id}"
}

resource "aws_apigatewayv2_route" "default" {
  api_id    = aws_apigatewayv2_api.api_gateway.id
  route_key = "$default"
  target    = "integrations/${aws_apigatewayv2_integration.lambda_hello.id}"
}

resource "aws_lambda_permission" "api_gw" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = module.ao-socket-lambda.arn
  principal     = "apigateway.amazonaws.com"
  source_arn    = "${aws_apigatewayv2_api.api_gateway.execution_arn}/*/*"
}

resource "aws_apigatewayv2_integration" "api_gw_interation" {
  api_id           = aws_apigatewayv2_api.api_gateway.id
  integration_type = "AWS_PROXY"
  integration_uri  = module.ao-socket-lambda.arn
}
