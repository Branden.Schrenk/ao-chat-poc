# A policy that allows AWS Lambda to assume a role
data "aws_iam_policy_document" "lambda_assume_role_policy" {
  version = "2012-10-17"
  statement {
    sid     = "AllowLambdaToAssumeRole"
    actions = ["sts:AssumeRole"]
    effect  = "Allow"
    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }
  }
}

# A policy that allows AWS API Gateway to assume a role
data "aws_iam_policy_document" "api_gateway_assume_role_policy" {
  version = "2012-10-17"
  statement {
    sid     = "AllowApiGatewayToAssumeRole"
    actions = ["sts:AssumeRole"]
    effect  = "Allow"
    principals {
      type        = "Service"
      identifiers = ["apigateway.amazonaws.com"]
    }
  }
}

# A policy to allow apigee to call the api gateway
data "aws_iam_policy_document" "api_gateway_policy" {
  version = "2012-10-17"
  statement {
    sid    = "ReadFromApigeeIps"
    effect = "Allow"
    principals {
      type        = "*"
      identifiers = ["*"]
    }
    actions = [
      "execute-api:Invoke",
      "execute-api:ManageConnections"
    ]
    resources = ["execute-api:/*"]
    condition {
      test     = "StringLike"
      variable = "aws:SourceIp"
      values   = lookup(var.apigee_ips, var.environment, lookup(var.apigee_ips, "default"))
    }
  }
}

# A policy to allow the lambda function to be invoked
data "aws_iam_policy_document" "api_gateway_invoke_policy" {
  version = "2012-10-17"
  statement {
    actions = [
      "lambda:InvokeFunction"
    ]
    effect = "Allow"
    resources = [
      module.ao-socket-lambda.arn
    ]
  }
}

# The JSON for the lambda execution_policy on ssm Parameter store
data "aws_iam_policy_document" "lambda-execution-role-policy-ao-socket" {
  version = "2012-10-17"

  statement {
    sid = "AllowLambdaFunctionToAccessParameterFromSsm"
    actions = [
      "ssm:GetParameter",
      "ssm:PutParameter",
      "ssm:AddTagsToResource"
    ]
    effect = "Allow"
    resources = [
      "arn:aws:ssm:${var.region}:${data.aws_caller_identity.current.account_id}:parameter/${module.resource_info.upper_resource_name}/*"
    ]
  }

  statement {
    sid    = "AllowLambdaFunctionToAccessDynamoDB"
    effect = "Allow"
    actions = [
      "dynamodb:DescribeTable",
      "dynamodb:Query",
      "dynamodb:UpdateItem",
      "dynamodb:DeleteItem",
      "dynamodb:DescribeStream",
      "dynamodb:GetRecords",
      "dynamodb:GetShardIterator",
      "dynamodb:ListStreams",
      "dynamodb:PutItem",
      "dynamodb:GetItem",
      "dynamodb:Scan",
      "dynamodb:BatchWriteItem",
    ]
    resources = [
      "arn:aws:dynamodb:${var.region}:${data.aws_caller_identity.current.account_id}:table/${module.socket_table.table.id}",
      "arn:aws:dynamodb:${var.region}:${data.aws_caller_identity.current.account_id}:table/${module.socket_table.table.id}/*"
    ]
  }

  statement {
    sid    = "AllowLambdaFunctionToInvoke"
    effect = "Allow"
    actions = [
      "execute-api:Invoke",
      "execute-api:ManageConnections"
    ]
    resources = [
      "*",
      "arn:aws:execute-api:*:*:**/@connections/*"
    ]
  }
}



