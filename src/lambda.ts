import {
    ApiGatewayManagementApiClient,
    PostToConnectionCommand,
    PostToConnectionCommandInput
} from "@aws-sdk/client-apigatewaymanagementapi";
import {
    APIGatewayProxyEvent,
    APIGatewayProxyResult,
    Callback,
    Context,
    Handler
} from "aws-lambda";


export const handler: Handler = async (event: APIGatewayProxyEvent, context: Context, callback: Callback): Promise<APIGatewayProxyResult> => {
    const connectionId: string = event.requestContext.connectionId || "";

    const domain = event.requestContext.domainName;
    const stage = event.requestContext.stage;
    const callbackUrl = `https://${domain}/${stage}`;
    const endpoint = 'https://' + event.requestContext.domainName + '/' + event.requestContext.stage;

    if (!event.requestContext && connectionId != "") {
        return {
            statusCode: 500,
            body: JSON.stringify({
                message: 'some error happened',
            }),
        };
    }

    try {
        const routeKey = event.requestContext.routeKey;
        const body = event.body;
        console.log("connectionId", connectionId);
        console.log("event: ", JSON.stringify(event));
        console.log("context: ", JSON.stringify(context));
        console.log("routeKey: ", routeKey);

        switch (routeKey) {
            case '$connect':
                return { statusCode: 200, body: 'Connected.' };
            case '$disconnect':
                // code
                return { statusCode: 200, body: 'Disconnect.' };
            default:
                console.log("body", body);
                console.log('endpoint ', callbackUrl)
                const apiGwClient = new ApiGatewayManagementApiClient({
                    endpoint: callbackUrl
                });

                const reqParam: PostToConnectionCommandInput = { ConnectionId: connectionId, Data: "Some Magic" };
                const request = new PostToConnectionCommand(reqParam);
                // % awscurl --service execute-api -X POST -d "hello3" https://hiprb0t4bf.execute-api.us-east-1.amazonaws.com/staging/@connections/IRGy9cV_IAMCE4w=

                try {
                    const res = await apiGwClient.send(request);
                    console.log(res);
                } catch (e) {
                    console.log(JSON.stringify(e))
                }
                return { statusCode: 200, body: 'data endpoint' };
        }
    }
    catch (err) {
        console.log(err);
        return {
            statusCode: 500,
            body: JSON.stringify({
                message: 'some error happened',
            }),
        };
    }
};


